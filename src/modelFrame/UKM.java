package modelFrame;

public class UKM {

    private String namaUnit;
    private Mahasiswa ketua;
    private Mahasiswa sekretaris;
    private Penduduk[] anggota;
    private final int MAX_ANGGOTA = 10;
    private int jumlahAnggota = 0;

    public UKM() {

    }

    public void setNamaUnit(String namaUnit) {
        this.namaUnit = namaUnit;
    }

    public void setKetua(Mahasiswa ketua) {
        this.ketua = ketua;
    }

    public void setSekretaris(Mahasiswa sekretaris) {
        this.sekretaris = sekretaris;
    }

    public void setAnggota(Penduduk[] anggota) {
        this.anggota = anggota;
    }

    public String getNamaUnit() {
        return namaUnit;
    }

    public Mahasiswa getKetua() {
        return ketua;
    }

    public Mahasiswa getSekretaris() {
        return sekretaris;
    }

    public boolean setAnggota(Penduduk[] anggota, int jumlahAnggota) {
        if (jumlahAnggota <= MAX_ANGGOTA) {
            this.anggota = anggota;
            this.jumlahAnggota = jumlahAnggota;
            return true;
        } else {
            return false;
        }
    }

    public void cetakAnggotaMahasiswa() {
        System.out.println("Kategori : Mahasiswa");
         System.out.println("------------------------------------------"
                + "-------------------------");
        System.out.println("Nama\tTempat,Tanggal Lahir\t\tNIM\t\tIuran");
         System.out.println("------------------------------------------"
                + "-------------------------");
        for (int i = 0; i < anggota.length; i++) {
            if (anggota[i] != null) {
                if (anggota[i] instanceof Mahasiswa) {
                    Mahasiswa a = (Mahasiswa) anggota[i];
                    System.out.print(""+a.getNama());
                    System.out.print("\t"+a.getTempatTanggalLahir());
                    System.out.print("\t\t"+a.getNim());
                    System.out.print("\t"+a.Iuran());
                    System.out.println("");
                }
            }
        }
    }

    public void cetakAnggotaMasyarakatSekitar() {
        System.out.println("");
        System.out.println("");
        System.out.println("");
        System.out.println("Kategori : Masyarakat Sekitar");
        System.out.println("------------------------------------------"
                + "-------------------------");
        System.out.println("Nama\tTempat,Tanggal Lahir\t\tNomor\t\tIuran");
        System.out.println("------------------------------------------"
                + "-------------------------");
        for (int j = 0; j < anggota.length; j++) {
            if (anggota[j] instanceof MasyarakatSekitar) {
                MasyarakatSekitar b = (MasyarakatSekitar) anggota[j];
                System.out.print("" + b.getNama());
                System.out.print("\t" + b.getTempatTanggalLahir());
                System.out.print("\t\t" + b.getNomor());
                System.out.print("\t" + b.Iuran());
                System.out.println("");
            }
        }
    }
}
