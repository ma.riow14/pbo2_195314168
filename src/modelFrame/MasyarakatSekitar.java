package modelFrame;

public class MasyarakatSekitar extends Penduduk {

    private String nomor;

    public MasyarakatSekitar() {

    }

    public MasyarakatSekitar (String nama, String tempatTanggalLahir) {
       super(nama,tempatTanggalLahir);
    }

    public void setNomor(String nomor) {
        this.nomor = nomor;
    }

    public String getNomor() {
        return nomor;
    }
    
    @Override
    public double Iuran(){
        double harga = Double.parseDouble(nomor);
        return harga / 10000;
    }

}
