package modelFrame;

public class Mahasiswa extends Penduduk {

    private String nim;

    public Mahasiswa() {

    }

    public Mahasiswa(String nama, String tempatTanggalLahir) {
       super(nama,tempatTanggalLahir);
    }

    public void setNim(String nim) {
        this.nim = nim;
    }

    public String getNim() {
        return nim;
    }
    
    @Override
    public double Iuran(){
        double harga = Double.parseDouble(nim);
        return harga / 10000;
        
    }

}
