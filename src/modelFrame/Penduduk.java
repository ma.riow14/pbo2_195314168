package modelFrame;

public abstract class Penduduk {

    protected String nama, tempatTanggalLahir;
    protected double iuran;

    public Penduduk() {
    }

    public Penduduk(String nama, String ttl) {
        this.nama = nama;
        this.tempatTanggalLahir = ttl;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public void setTempatTanggalLahir(String tempatTanggalLahir) {
        this.tempatTanggalLahir = tempatTanggalLahir;
    }

    public String getNama() {
        return nama;
    }

    public String getTempatTanggalLahir() {
        return tempatTanggalLahir;
    }
    
    public abstract double Iuran();
    
}
