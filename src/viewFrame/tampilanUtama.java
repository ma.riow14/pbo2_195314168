package viewFrame;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import modelFrame.Mahasiswa;
import modelFrame.MasyarakatSekitar;
import modelFrame.Penduduk;
import modelFrame.UKM;

public class tampilanUtama extends JFrame implements ActionListener {

    private JMenuBar menuBar;
    private JMenuItem menuItemEdit_tambahMahasiswa;
    private JMenuItem menuItemEdit_tambahMasyarakat;
    private JMenuItem menuItemEdit_tambahUKM;
    private JMenuItem menuItemFile_tampilkanData;
    private JMenuItem menuItemFile_keluar;
    private JMenu menu_File;
    private JMenu menu_Edit;
    private JMenu menu_Help;

    public static UKM ukm = new UKM();
    public static Mahasiswa mhs = new Mahasiswa();
    public static MasyarakatSekitar masya = new MasyarakatSekitar();
    public static Penduduk[] anggota = new Penduduk[10];
    public static int jumlah = 0;

    public tampilanUtama() {
        initComponents();
    }

    private void initComponents() {

        menuBar = new JMenuBar();
        menu_File = new JMenu("File");
        menu_Edit = new JMenu("Edit");
        menu_Help = new JMenu("Help");

        menuBar.add(menu_File);
        menuBar.add(menu_Edit);
        menuBar.add(menu_Help);
        this.setJMenuBar(menuBar);

        menuItemEdit_tambahMahasiswa = new JMenuItem("Tambah Mahasiswa");
        menuItemEdit_tambahMasyarakat = new JMenuItem("Tambah Masyarakat "
                + "Sekitar");
        menuItemEdit_tambahUKM = new JMenuItem("Tambah UKM");
        menuItemFile_tampilkanData = new JMenuItem("Tampilkan Data");
        menuItemFile_keluar = new JMenuItem("Keluar");

        menu_Edit.add(menuItemEdit_tambahMahasiswa);
        menu_Edit.add(menuItemEdit_tambahMasyarakat);
        menu_Edit.add(menuItemEdit_tambahUKM);

        menu_File.add(menuItemFile_tampilkanData);
        menu_File.add(menuItemFile_keluar);
        menuBar.add(menu_File);
        menuBar.add(menu_Edit);
        menuBar.add(menu_Help);

        menuItemFile_keluar.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.exit(0);
            }
        });


        menuItemEdit_tambahMahasiswa.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                tambahMahasiswa addMhs = new tambahMahasiswa();
                addMhs.setSize(400, 300);
                addMhs.setVisible(true);
            }
        });

        menuItemEdit_tambahMasyarakat.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                tambahMasyarakatSekitar addMasyarakat = new tambahMasyarakatSekitar();
                addMasyarakat.setSize(400, 300);
                addMasyarakat.setVisible(true);
            }
        });

        menuItemEdit_tambahUKM.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                tambahUKM addUKM = new tambahUKM();
                addUKM.setSize(400, 460);
                addUKM.setVisible(true);
            }
        });

        java.awt.EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                tampilanUtama main = new tampilanUtama();
                main.setSize(400, 500);
                main.setVisible(true);
                main.setResizable(false);
                main.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            }
        });
    }

    @Override
    public void actionPerformed(ActionEvent e) {

    }
}
